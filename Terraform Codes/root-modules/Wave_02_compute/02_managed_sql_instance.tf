variable "sql_vms" {
    type = list(object({
        hostname                                    = string
        tags                                        = map(string)
        resource_group_name                         = string
        location                                    = string
        keyvault = object({
            name                                    = string
            resource_group_name                     = string
        })
        vm_data = object({
            admin_password_secret                   = string
            admin_username_secret                   = string
            audit_diagnostics                       = bool
            audit_diagnostics_sa_type               = string
            version                                 = string
        })
        databases = list(object({
            name                            = string
        }))
    }))
}

module "managed_sql" {
    source                                          = "../../modules/azurerem_managed_sql_server"
    for_each                                        = { for sql in toset( var.sql_vms ): sql.hostname => sql }
        hostname                                    = each.key
        resource_group_name                         = each.value.resource_group_name
        keyvault                                    = each.value.keyvault
        vm_data                                     = each.value.vm_data
        databases                                   = each.value.databases
        tags                                        = merge(var.overall_tags, each.value.tags)

}

 