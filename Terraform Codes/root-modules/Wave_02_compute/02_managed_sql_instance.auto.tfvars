sql_vms = [{
    hostname                                        = "applnserver"
    tags                                            = null
    resource_group_name                             = "rg1"
    location                                        = null
    keyvault = {
        name                                        = "keyvault1111111111"
        resource_group_name                         = "keyvault-rg"
    }
    vm_data = {
        admin_password_secret                       = "passwordsecret"
        admin_username_secret                       = "usernamesecret"
        audit_diagnostics                           = true
        audit_diagnostics_sa_type                   = "Standard_LRS"
        version                                     = "12.0"
    }
    databases = [{
        name                                        = "database1"
    },
    {
        name                                        = "database2"
    }]

}]


