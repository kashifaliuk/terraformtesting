#####################################################################
#                                                                   #
#         Data Sources (resource_group; subnet; keyvault)           #
#                                                                   #
#####################################################################

data "azurerm_resource_group" "rg" {
    name                        = var.resource_group_name
}

data "azurerm_key_vault" "kv" {
    name                        = var.keyvault.name
    resource_group_name         = var.keyvault.resource_group_name
}

data "azurerm_key_vault_secret" "kv_username" {
    name                        = var.vm_data.admin_username_secret
    key_vault_id                = data.azurerm_key_vault.kv.id
}

data "azurerm_key_vault_secret" "kv_password" {
    name                        = var.vm_data.admin_password_secret
    key_vault_id                = data.azurerm_key_vault.kv.id
}



#####################################################################
#                                                                   #
#                          Auditing SA                              #
#                                                                   #
#####################################################################

resource "random_id" "vm-sa" {
    keepers = {
    hostname                    = var.hostname
    }

    byte_length                 = 6
}

resource "azurerm_storage_account" "sa" {
    name                        = "auditing${lower(random_id.vm-sa.hex)}"
    resource_group_name         = data.azurerm_resource_group.rg.name
    location                    = coalesce(var.location, data.azurerm_resource_group.rg.location)
    account_tier                = element(split("_", var.vm_data.audit_diagnostics_sa_type), 0)
    account_replication_type    = element(split("_", var.vm_data.audit_diagnostics_sa_type), 1)
    tags                        = var.tags
}


#####################################################################
#                                                                   #
#                              SQL_Machine                          #
#                                                                   #
#####################################################################

resource "azurerm_sql_server" "sqlserver" {
    name                          = var.hostname
    resource_group_name           = data.azurerm_resource_group.rg.name
    location                      = coalesce(var.location, data.azurerm_resource_group.rg.location)
    version                       = var.vm_data.version
    administrator_login           = data.azurerm_key_vault_secret.kv_username.value
    administrator_login_password  = data.azurerm_key_vault_secret.kv_password.value
}   


module "databases"{
    source                      = "../azurerm_managed_sql_database"
    for_each                    = { for database in toset(var.databases) : database.name => database }
        name                    = each.key
        resource_group_name     = data.azurerm_resource_group.rg.name
        location                = coalesce(var.location, data.azurerm_resource_group.rg.location)
        server_name             = azurerm_sql_server.sqlserver.name
        storage_endpoint        = azurerm_storage_account.sa.primary_blob_endpoint
        storage_access_key      = azurerm_storage_account.sa.primary_access_key
}
