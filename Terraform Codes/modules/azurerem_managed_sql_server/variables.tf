variable "hostname" {
    type        = string
}

variable "resource_group_name" {
    type        = string
}

variable "location" {
    type        = string
    default     = ""
}

variable "keyvault" {
    type        = map(string)
}

variable "vm_data" {
    description = "All parameters to setup the VM."
    type        = object({
        admin_password_secret                       = string
        admin_username_secret                       = string
        audit_diagnostics                           = bool
        audit_diagnostics_sa_type                   = string
        version                                     = string
    })
}

variable "databases" {
    type = list(object({
                            name              = string
                        }))
}

variable "tags" {
    type        = map(string)
}