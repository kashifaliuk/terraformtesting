resource "azurerm_sql_database" "database" {
  name                                      = var.name
  resource_group_name                       = var.resource_group_name
  location                                  = var.location
  server_name                               = var.server_name

  extended_auditing_policy {
    storage_endpoint                        = var.storage_endpoint
    storage_account_access_key              = var.storage_access_key
    storage_account_access_key_is_secondary = true
    retention_in_days                       = 6
  }

}


