variable "name" {
    type        = string
}

variable "resource_group_name" {
    type        = string
}

variable "location" {
    type        = string
}

variable "server_name" {
    type        = string
}

variable "storage_endpoint" {
    type        = string
}

variable "storage_access_key" {
    type        = string
}