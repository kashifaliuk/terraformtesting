Author:
* Kashif Ali

Date:    19.02.2021

#The folders and their functionality is mentioned below


###Terraform Codes
	This folder contains the Terraform codes for the Web Server and DB architecture. We have 2 sub folders named as modules and root-modules.
	1. Modules - Contains the source for the modules that are called in the Root-Modules folder
	2. Root-Modules - Contains two subfolders Wave_01_landing_zone and Wave_02_compute. As we are first deploying the Landing_Zone and then the Compute
	Note:
		1. Provider block doesnot contain any secrets as we are assuming that it will be deployed using Azure DevOps and Service connection can be used
		2. We have not written the code for Key Vault ,Key Vault Secrets and Key Vault Access Policy. These are manually created and the are being referenced in terraform using data block




###Packer
	This folder contains the Json file for creating a Windows Image on Azure
	Note:
		1. Builders block does not contain any secrets as it will be deployed using Azure DevOps and Service Connection

		